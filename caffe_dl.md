星期三, 08. 七月 2015 05:35下午 

**Caffe** 

*caffe*是一个深度学习的框架.

**Blob**  

从数学上来说*Blob*是一个*n*维的连续的数组. *Blob*实现数据在*CPU*和*GPU*之间的同步, 提高了内存利用效率.

有两种方式访问存储在*CPU*或*GPU*上数据: 
const Dtype\* cpu_data()  const;
Dtype\* mutable_cpu_data();

*Blob*利用*SyncedMem*类同步*CPU*和*GPU*间的数据, 隐藏同步的细节, 最小化数据的传递损耗.

**Layer**

*Layer*是*Modal*的核心, 大部分学习任务都是在*Layer*上完成的.

每个*Layer*都会定义三个重要的计算: *==setup==*, *==forward==*, *==backward==*.

*Layers*在整个神经网络的处理过程中有两个重要的职责:
> 前向传递: 获取输入并计算出输出.
反向传递: 根据输出结果和输入计算出梯度并反向传回上一层.

**Net**

*net* 是连接着的*Layers*的集合, 通常由文本模块语言描述.
name: "LogReg"
layer {
  name: "mnist"
  type: "Data"
  top: "data"
  top: "label"
  data_param {
    source: "input_leveldb"
    batch_size: 64
   }
}
layer {
  name: "ip"
  type: "InnerProduct"
  bottom: "data"
  top: "ip"
  inner_product_param {
    num_output: 2
  }
}
layer {
  name: "loss"
  type: "SoftmaxWithLoss"
  bottom: "ip"
  bottom: "label"
  top: "loss"
}

*net* 的构建是==设备无关==的.

**Forward and Backward**

*forward* 从底向上, 前一层的输出作为后一层的输入, 到最顶层结束.
*backward* 在计算出误差之后开始反向, 逐层计算梯度(如何修正 ? ).

**Loss**

 *Caffe* 是由*loss*函数驱动的, 因为学习的目的是找到一系列的权值来最小化*loss*函数. *one-versus-all*的分类任务通常选择*SoftmaxWithLoss*函数
layer {
  name: "loss"
  type: "SoftmaxWithLoss"
  bottom: "pred"
  bottom: "label"
  top: "loss"
} 

带有*Loss*后缀的*Layers*有一个隐式的*loss_weight*: 1 for the first top blob (and loss_weight: 0 for any additional tops); other layers have an implicit loss_weight: 0 for all tops. 

*caffe*的*==final loss==*是整个网络所有*weighted loss*的和.
loss := 0
for layer in layers:
    for top, loss_weight in layer.tops, layer.loss_weights:
        loss += loss_weight * sum(top)

**Solver**

*Caffe*的*solvers*包括:*Stochastic Gradient Descent (==SGD==), Adaptive Gradient (==ADAGRAD==), and Nesterov’s Accelerated Gradient (==NESTEROV==).*

*The solver*

1.组织优化计划并创建学习网络.
2. 通过*forward/backward*迭代优化并更新权值.
3. 阶段性评估网络.
4. 跟踪整个优化过程的*modal*和*solver*的状态.
where each ==iteration==
 1. 前向计算输出和误差, 反向计算梯度.
 2. 根据*solver method*将梯度更新到参数中.
 3. 在学习后的模型中改变权值, 根据学习的速率, 历史和方法更新*solver method*的状态.
